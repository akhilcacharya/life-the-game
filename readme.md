##What's working

This list will serve as a simple indicator of what is working and what has changed. In case you're too lazy to look at the commit log. 

* Quick Twitter Login 

* Twitter Account Creation

* Create and Read functionality 

* MongoDB models look about done. 

* Frontend boilerplate (login, logout, "Hello" message, etc)

* Decent looking header

* Started "create" form. 

* Implemented basic Angular Logic in form validation

* HTTP Post now actually *works* - put Bodyparser at the top people! 