module.exports = function(config, passport, TwitterStrategy, mongoose, User, Thing) {
    //Auth 
    passport.use(new TwitterStrategy({
        consumerKey: config.get("twitter:consumer_key"),
        consumerSecret: config.get('twitter:secret_key'),
        callbackURL: config.get('twitter:callback_url'),
    }, function(token, tokenSecret, profile, done) {
        //Create User
        var name = profile.name;
        User.findOne({
            oauth_key: token
        }, function(err, user) {
            if (user) {
                console.log('Found user');
                done(null, user.oauth_key);
            } else {

                var tags = ["First!"];

                var firstThing = new Thing({
                    name: "First Quest!",
                    description: "Find your way around the app",
                    isActive: true,
                    tags: tags,
                    difficulty: 3,
                    gold_value: 15,
                });

                var things = [];
                things[0] = firstThing;

                var newUser = new User({
                    oauth_key: token,
                    username: profile.username,
                    profile_data: {
                        name: profile.displayName,
                        icon: profile.photos,
                        email: profile.emails,
                    },
                    things: things,
                    gold: 0,
                });
                newUser.save(function(err) {
                    if (!err) {
                        console.log("New User Saved");
                        done(null, newUser.oauth_key);
                    } else {
                        throw err;
                    }
                });
            }
        });
    }));

    var routes = {
        login: passport.authenticate('twitter', {
            session: true
        }),
        callback: passport.authenticate('twitter', {
            successRedirect: "/app", 
            failureRedirect: "/"
        }),
        
        index: function(req, res){
            if(req.user){
                res.redirect("/app"); 
            }else{
                res.render('index')
            }
        }, 

        main: function(req, res) {
            if (req.user) {
                res.render('app', {
                    user: req.user
                });
            } else {
                res.redirect('/');
            }
        },

        quests: function(req, res){
            if (req.user) {
                res.render('quest', {user: req.user});
            } else {
                res.redirect('/');
            }
        }, 

        create: function(req, res) {
            if (req.user) {
                res.render('create', {user: req.user});
            } else {
                res.redirect('/');
            }
        },

        created: function(req, res){
            if(req.user){
                console.log(req.body); 
                //res.send({status: 200}); 
                res.render('app', {user: req.user})
            }else{
                res.send(JSON.stringify({status: 401})); 
            }
        }, 

        profile: function(req, res) {
            var username = req.params.id;
            User.findOne({
                username: username
            }, function(err, user) {
                if (!user) {
                    res.send('404');
                } else {
                    res.render("profile", {
                        user: user
                    })
                }
            });
        },

        logout: function(req, res) {
            req.logout();
            res.redirect('/');
        }
    }
    return routes;
};
