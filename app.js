/******************************************
 * Author: Akhil Acharya
 * Project: Life, the Game (working title)
 * Started: 11/9/13
 *******************************************/


var express = require('express');

//Configuration Data
var config = require('nconf');
config.file({
    file: __dirname + "/config.json"
});

//Auth 
var passport = require('passport');
var twitter = require('passport-twitter').Strategy;

//App
var app = module.exports = express.createServer();

//DB
var mongoose = require('mongoose');
var db_url = String(config.get('db:base_url')) + String(config.get('db:username')) + ":" + String(config.get('db:password')) + String(config.get('db:path'));

mongoose.connect(db_url);
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection error'));

db.once('open', function callback() {
    console.log("Connected to remote MongoDB");
})

//Models
var Thing = require('./models/ThingSchema.js')(mongoose);
var User = require('./models/UserSchema.js')(mongoose);

//Controllers
var game = require('./controllers/game.js')(config, passport, twitter, mongoose, User, Thing);

// Configuration
passport.serializeUser(function(key, done) {
    done(null, key);
});

passport.deserializeUser(function(key, done) {
    User.findOne({
        oauth_key: key
    }, function(err, user) {
        done(err, user);
    });
});

app.configure(function() {
        app.use(express.bodyParser());
    app.use(express.cookieParser());
    app.use(express.session({
        secret: "I'm actually a redhead"
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');

    app.use(express.methodOverride());
    app.use(app.router);
    app.use(express.static(__dirname + '/public'));
});

app.configure('development', function() {
    app.use(express.errorHandler({
        dumpExceptions: true,
        showStack: true
    }));
});

app.configure('production', function() {
    app.use(express.errorHandler());
});

/** Web Routes **/
//Pages
app.get('/', game.index);
app.get('/app', game.main);
app.get('/app/quests', game.quests);

//Create
app.get('/app/create', game.create);
app.post('/app/created', game.created)


//Profiles
app.get('/p/:id', game.profile);


//Authentication
app.get('/auth/twitter', game.login);
app.get('/auth/twitter/callback', game.callback);
app.get('/logout', game.logout);




/** REST Routes **/
/** HTTP GET**/
/** HTTP POST**/






app.listen(8081, function() {
    if (!config.get('production')) {
        console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
        console.log("Currently in testing mode");
    }
});
