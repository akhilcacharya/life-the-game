module.exports = function(mongoose){
	var Schema = mongoose.Schema; 
	var userSchema = new Schema({
		//Profile data
		oauth_key: String, 
		username: String, 
		profile_data: {
			name: String, 
			icon: Array, 
			email: Array, 
		}, 
		things: Array, 
		gold: Number, 
	}); 

	var userModel = mongoose.model("User", userSchema); 

	return userModel; 
}