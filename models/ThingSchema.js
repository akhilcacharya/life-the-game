module.exports = function(mongoose){
	var Schema = mongoose.Schema; 

	var thingSchema = new Schema({
		name: String, 
		description: String, 
		isActive: Boolean, 
		tags: Array, 
		difficulty: Number, 
		reward_value: Number, 
		penalty_value: Number, 
	}); 

	var Thing = mongoose.model('Thing', thingSchema); 
	return Thing; 
}; 